
# Passpocket

It is a password manager built with Java as backend language along with Javafx (UI design) and Microsoft SQL server as database. AES 256 encryption algorithm has been implemented.

Here we have two perspective    
    1. User     
    2. Admin    
Any user can register and log in to store credentials. Benefits of insertion, deletion, modification & searching have been provided properly.   
From an admin perspective, an admin can perform all the functions same as user moreover admin can view statistics. 



## Contributing

Contributions are always welcome!

Please adhere to this project's `manners and etiquette`.


## Features

- Login & Registration
- Encryption
- Personal notes
- Password generator
- Cross platform


## Feedback

If you have any feedback, please reach out to us at http://scr.im/tasnifabirg

